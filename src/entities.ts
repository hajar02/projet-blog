export interface Article {
    id?:number;
    title:string;
    description:string;
    date:string;
    image: string;
    creator: string;
    idCategory: number;
}

export interface Category {
    id?:number;
    categoryName:string;
}
