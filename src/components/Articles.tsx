
import { useEffect, useState } from 'react';
import React from 'react';
import { Article } from '@/entities';
import { fetchAllArticle } from '@/article-service';
import ItemArticle from "@/components/ItemArticle";

export default function Articles() {

    const [articles, setArticles]=useState<Article[]>([]);

useEffect(()=>{
fetchAllArticle().then(data=>{
    setArticles(data);
});
},[])

  return (
    <div>
      <div className="container">

        {articles.map((item) => 
         <ItemArticle key={item.id} article={item}/>
         )} 

      </div>
    </div>
  );

}
