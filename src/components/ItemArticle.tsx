import { useEffect, useState } from 'react';
import React from 'react';
import { Article } from '@/entities';
import { deleteArticle } from "@/article-service";
import Link from "next/link";
import { useRouter } from "next/router";


interface Props {
    article: Article;
}

export default function ItemArticle({ article }: Props) {

  const router = useRouter();
  const [isDeleted, setIsDeleted] = useState(false); // Aggiungi lo stato locale

  async function remove() {
    await deleteArticle(article.id);
    setIsDeleted(true); // Imposta lo stato su true quando l'articolo viene cancellato
    router.push('/');
  }

  // Ricarica la pagina quando lo stato cambia
  useEffect(() => {
    if (isDeleted) {
      window.location.reload();
    }
  }, [isDeleted]);

  return (
    <div key={article.id}>
      <div className="card">
        <div className="card-header">
          <img src={article.image} alt="rover" />
        </div>
        <div className="card-body">
          <h4>{article.title}</h4>
          <p>{article.description.substring(0, 70)}...</p>
          <p>{article.date}</p>
          <div className="user">
            <div className="user-info">
  <img src='https://icons-for-free.com/iconfiles/png/512/person+profile+user+icon-1320184030388165551.png' alt='user'/>
              <small>{article.creator}</small>
            </div>
          </div>
          <div className='buttons'>
            <button> <Link href={"/article/" + article.id}>Consulter</Link></button>
            <button onClick={remove}>X</button>
          </div>
        </div>
      </div>
    </div>
  );
}
