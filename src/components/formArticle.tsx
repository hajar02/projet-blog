import { Article, Category } from "@/entities"
import { FormEvent, useState } from "react"

interface Props {
    onSubmit:(article:Article) => void;
    edited?:Article;
}

export default function FormArticle({onSubmit, edited}:Props) {

    const [errors, setErrors] = useState('');

    const [article, setArticle] = useState<Article>(edited?edited:{
        title:"",
        description:"",
        date:"",
        image: "",
        creator: "",
        idCategory: 2,
    });

    console.log(edited);

    function handleChange(event: any) {
        setArticle({
            ...article,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        try {
            onSubmit(article);

        } catch(error:any) {
            if(error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }

    }


    return (

        <form onSubmit={handleSubmit}>

            {errors && <p>{errors}</p>}


            <div className="econtainer">
        <div className="ecard">
            <a className="eedit">Form</a>
            <div className="einputBox">
            <input type="text" name="title" value={article.title} onChange={handleChange} placeholder="Inserisci il titolo" required />
            </div>

            <div className="einputBox">
            <input type="text" name="description" value={article.description} placeholder="Inserisci la descrizione" onChange={handleChange} />
            </div>

            <div className="einputBox">
            <input type="text" name="creator" value={article.creator} placeholder="Inserisci il tuo nome" onChange={handleChange} />
            </div>

            <div className="einputBox">
            <input type="text" name="image" value={article.image} placeholder="Inserisci link dell'immagine" onChange={handleChange} />
            </div>

            <div className="einputBox">
            <input type="date" name="date" value={article.date} onChange={handleChange} />
            </div>

            <div className="einputBox">
               {/*<select name="" id=""> 
                    <option value={1}>Une categorie</option>
    </select> */} 
            <input type="text" name="number" value={article.idCategory} onChange={handleChange} />
            </div>

            <button className="eenter">Submit</button>

        </div>
    </div>

        </form>
    )
}

