
import React from 'react';
import Link from 'next/link';


export default function Header() {

    return (  
    
    <header>

        <nav className="navbar navbar-expand-lg bg-body-tertiary menuu">
            <div>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav" style={{width: "100%"}}>
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link active" aria-current="page" href="/" style={{color: "white", textAlign: "center"}}>Home</a>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link active" href={"/article/add"} style={{color: "white", textAlign: "center"}}> New article</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <section className="header" id="description">

            <h1>Maroc</h1>
            
        </section>

    </header>
    
     );

}
