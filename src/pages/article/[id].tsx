import FormArticle from "@/components/formArticle";
import { deleteArticle, fetchOneArticle, updateArticle } from "@/article-service";
import { Article } from "@/entities";
import Header from "@/components/header";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Link from "next/link";

export default function ArticlePage() {
    const router = useRouter();
    const { id } = router.query;
    const [article, setArticle] = useState<Article>();
    const [showEdit, setShowEdit] = useState(false);

    useEffect(() => {
        if (!id) {
            return;
        }
        fetchOneArticle(Number(id))
            .then(data => setArticle(data))
            .catch(error => {
                console.log(error);
                if (error.response.status == 404) {
                    router.push('/404');
                }
            });

    }, [id]);

    async function remove() {
        await deleteArticle(id);
        router.push('/');
    }

    async function update(article: Article) {
        const updated = await updateArticle(article);
        setArticle(updated);
    }

    function toggle() {
        setShowEdit(!showEdit);
    }

    if (!article) {
        return <p>Loading...</p>
    }

    return (
        <>
            <Header />


            <div className="acontainer">
                <div className="aimage-container">
                    <img src={article.image} alt="Immagine" />
                </div>
                <div className="atext-container">
                    <h2>{article.title}</h2>
                    <p>{article.description}</p>
                    <p>Autor: {article.creator}</p>
                    <p>Data: {article.date}</p>
                    <div className="buttonss">
                <button onClick={remove}>Delete</button>
                <button onClick={toggle}>Edit</button>
            </div>
                </div>
            </div>

            {showEdit &&
                <>
                    <FormArticle edited={article} onSubmit={update} />
                </>
            }

        </>
    );

}