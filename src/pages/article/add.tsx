import FormArticle from "@/components/formArticle";
import { postArticle } from "@/article-service";
import { Article } from "@/entities";
import { useRouter } from "next/router";
import Header from "@/components/header";


export default function AddArticle() {
    const router = useRouter();

    async function addArticle(article: Article) {
        const added = await postArticle(article);
        router.push('/article/' + added.id);
    }

    return (
    <>
        <Header />
        
        <h1 style={{ textAlign: "center", marginTop: "5%" }}>Add Article</h1>
            <FormArticle onSubmit={addArticle} />
        </>
    );
}