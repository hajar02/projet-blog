import axios from "axios";
import { Article } from "./entities";

/**
 * On crée ici les différentes fonctions qui permettront de faire les requêtes HTTP vers l'API Rest en
 * Symfony. On aura généralement les fonctions correspondantes à ce qu'on a définit dans le contrôleur
 * de l'entité ciblée
 */

export async function fetchAllArticle() {
    const response = await axios.get<Article[]>('http://localhost:8000/api/article');
    return response.data;
}

export async function fetchOneArticle(id:number) {
    const response = await axios.get<Article>('http://localhost:8000/api/article/'+id);
    return response.data;
}

export async function postArticle(article:Article) {
    const response = await axios.post<Article>('http://localhost:8000/api/article', article);
    return response.data;
}


export async function updateArticle(article:Article) {
    const response = await axios.put<Article>('http://localhost:8000/api/article/'+article.id, article);
    return response.data;
}

export async function deleteArticle(id:any) {
    await axios.delete('http://localhost:8000/api/article/'+id);
}
